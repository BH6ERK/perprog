﻿using System.Collections.Generic;

namespace PERPROGFF
{
    class Band
    {
        private string bandName;
        private List<string> genres;
        private string origin;
        private List<string> members;
        private List<string> pastMembers;
        private string website;
        private List<string> labels;
        

        public string BandName
        {
            get
            {
                return bandName;
            }

            set
            {
                bandName = value;
            }
        }

        public List<string> Genres
        {
            get
            {
                return genres;
            }

            set
            {
                genres = value;
            }
        }

        public string Origin
        {
            get
            {
                return origin;
            }

            set
            {
                origin = value;
            }
        }

        public List<string> Members
        {
            get
            {
                return members;
            }

            set
            {
                members = value;
            }
        }

        public string Website
        {
            get
            {
                return website;
            }

            set
            {
                website = value;
            }
        }

        public List<string> Labels
        {
            get
            {
                return labels;
            }

            set
            {
                labels = value;
            }
        }

        public List<string> PastMembers
        {
            get
            {
                return pastMembers;
            }

            set
            {
                labels = pastMembers;
            }
        }

        public Band()
        {
            bandName = string.Empty;
            genres = new List<string>();
            origin = "";
            members = new List<string>();
            pastMembers = new List<string>();
            website = "";
            labels = new List<string>();
        }
    }
}
