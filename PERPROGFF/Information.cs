﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PERPROGFF
{
    public class Information : ObservableObject
    {
        int progress;
        int finishedSubtask;
        string urlcount;
        string processname;
        string progresstext;

        public int Progress
        {
            get
            {
                return progress;
            }

            set
            {
                progress = value;
            }
        }

        public string UrlCount
        {
            get
            {
                return urlcount;
            }

            set
            {
                urlcount = value;
            }
        }

        public string Processname
        {
            get
            {
                return processname;
            }

            set
            {
                processname = value;
            }
        }

        public string Progresstext
        {
            get
            {
                return progresstext;
            }

            set
            {
                progresstext = value;
            }
        }

        public Information(string urlcount, string processname, string progresstext)
        {
            this.progress = 0;
            this.urlcount = urlcount;
            this.processname = processname;
            this.progresstext = progresstext;
            this.finishedSubtask = 0;
        }

        public void IncrementProgress()
        {
            finishedSubtask += 1;
            this.Progress = finishedSubtask * 100 / (int.Parse(urlcount));
            this.Progresstext = progress.ToString() + " %";
            this.RaisePropertyChanged("Progress");
            this.RaisePropertyChanged("Progresstext");
        }

        public void NullifyProgress()
        {
            this.finishedSubtask = 0;
            this.Progress = 0;
            this.Progresstext = Progress.ToString() + " %";
            this.RaisePropertyChanged("Progress");
        }

        public void UrlCountReady(string urlcount)
        {
            this.UrlCount = urlcount;
            this.RaisePropertyChanged("URLCount");
        }

        public void ProcessChanged(string newprocessname)
        {
            this.Processname = newprocessname;
            this.RaisePropertyChanged("Processname");
        }
    }
}