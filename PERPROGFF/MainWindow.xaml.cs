﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace PERPROGFF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel VM;
        Solution solution;
        PathPack paths;
        private const string addressForCheck = "www.google.com";

        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VM = new ViewModel();
            this.DataContext = VM;
            paths = new PathPack();

            SetUIAvailabilities(1);
        }

        private async void run_Parallel_Solution_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!paths.IsPackReady())
            {
                System.Windows.MessageBox.Show("One or more paths were not set correctly.", "Invalid paths");
            }
            else if (!CheckConnection(addressForCheck))
            {
                System.Windows.MessageBox.Show("No internet connection!", "No internet");
            }
            else
            {
                try
                {
                    SetUIAvailabilities(2);

                    switch (auto_taskcount_checkbox.IsChecked)
                    {
                        case true:
                            solution = new Solution(Environment.ProcessorCount, int.Parse(textbox_delay.Text), VM.InformationForUser, paths);
                            await Task.Run(() => solution.RunSolution());
                            break;

                        case false:

                            solution = new Solution(int.Parse(task_count_textbox.Text), int.Parse(textbox_delay.Text), VM.InformationForUser, paths);
                            await Task.Run(() => solution.RunSolution());
                            break;
                    }

                    SetUIAvailabilities(3);
                    System.Windows.MessageBox.Show("Done! See the log for further information.");
                }
                catch (Exception exception)
                {
                    if (exception is FormatException)
                    {
                        System.Windows.MessageBox.Show("Please fill every box.", "FormatException");
                        SetUIAvailabilities(1);
                    }
                    else if (exception is ArgumentNullException)
                    {
                        System.Windows.MessageBox.Show("One or more paths were invalid.", "ArgumentNullException");
                        SetUIAvailabilities(1);
                    }
                    else if (exception is System.IO.IOException)
                    {
                        System.Windows.MessageBox.Show("Exception thrown while merging JSON files. Please delete generated files and restart the application for a new run.", "IOException");
                        SetUIAvailabilities(3);
                    }
                }
            }
        }

        private void CheckBoxChanged(object sender, RoutedEventArgs e)
        {
            switch ((sender as System.Windows.Controls.CheckBox).IsChecked)
            {
                case true:
                    task_count_textbox.Text = string.Empty;
                    task_count_textbox.IsEnabled = false;
                    break;

                case false:
                    task_count_textbox.IsEnabled = true;
                    break;
            }
        }

        private void set_output_path_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();

            folderBrowserDialog.Description = "Select output path:";

            paths.Downloadpath = folderBrowserDialog.SelectedPath;
        }

        private void set_input_path_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();

            paths.Urlfilepath = fileDialog.FileName;
        }

        private void threadCountTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[1-9]");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void textbox_delay_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]");
            e.Handled = !regex.IsMatch(e.Text);
        }

        //1=> initial state, 2=> running, 3=> finished
        private void SetUIAvailabilities(int phase)
        {
            switch (phase)
            {
                case 1:
                    start_over_button.IsEnabled = false;
                    task_count_textbox.IsEnabled = true;
                    run_Parallel_Solution_Button.IsEnabled = true;
                    textbox_delay.IsEnabled = true;
                    auto_taskcount_checkbox.IsEnabled = true;
                    set_input_path.IsEnabled = true;
                    set_output_path.IsEnabled = true;
                    break;

                case 2:
                    start_over_button.IsEnabled = false;
                    task_count_textbox.IsEnabled = false;
                    run_Parallel_Solution_Button.IsEnabled = false;
                    textbox_delay.IsEnabled = false;
                    auto_taskcount_checkbox.IsEnabled = false;
                    set_input_path.IsEnabled = false;
                    set_output_path.IsEnabled = false;
                    break;

                case 3:
                    start_over_button.IsEnabled = true;
                    task_count_textbox.IsEnabled = false;
                    run_Parallel_Solution_Button.IsEnabled = false;
                    textbox_delay.IsEnabled = false;
                    auto_taskcount_checkbox.IsEnabled = false;
                    set_input_path.IsEnabled = false;
                    set_output_path.IsEnabled = false;
                    break;
            }
        }

        private void start_over_button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.Application.Restart();
            Environment.Exit(0);
        }

        private void help_button_Click(object sender, RoutedEventArgs e)
        {
            new HelpWindow().ShowDialog();
        }

        private bool CheckConnection(string address)
        {
            try
            {
                System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = pingSender.Send(address);

                if (reply.Status == System.Net.NetworkInformation.IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }           
        }
    }
}
