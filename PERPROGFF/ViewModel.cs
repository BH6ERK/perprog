﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PERPROGFF
{
    class ViewModel
    {
        private Information informationForUser;

        public Information InformationForUser
        {
            get
            {
                return informationForUser;
            }
            
            private set
            {
                informationForUser = value;
            }
        }

        public ViewModel()
        {           
            informationForUser = new Information("---","Wating to start...","0 %");
        }
        
    }
}
