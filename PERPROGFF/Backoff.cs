﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PERPROGFF
{
    public static class BackOff
    {
        public static void Backoff(int amount)
        {
            Thread.Sleep(amount);
        }
    }
}
