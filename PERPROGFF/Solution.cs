﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace PERPROGFF
{
    class Solution
    {
        private PathPack paths;

        private const int maxTaskToDownload = 2;
        private const int maxTry = 3;
        private int downloadDelay;

        private const string folderForJsonFiles = @"\json\";
        private const string folderForLogFiles = @"\logfiles\";
        private const string downloadedFileFolderName = @"\files\";
        private const string mainFolderName = @"\PERPROGFF_output\";
        private const string separatedJsonFileName = @"json_part_";
        private const string finalJsonFileName = @"bands_merged.json";
        private const string logfileName = @"log.log";

        private List<string> URLs;
        private int tasksToStart;
        private Stopwatch stopwatch;

        private int successfullyDownloaded;
        private int successfullyParsed;

        object lockobj = new object();

        private Information informationForUser;
        private ConcurrentQueue<string> downloadedFiles;

        /// <summary>
        /// A semaphore for controlling download requests
        /// </summary>
        private SemaphoreSlim semaphore;
        private int semaphoreCount;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tasksToStart"></param>
        /// <param name="informationForUser"></param>
        /// <param name="paths"></param>
        public Solution(int tasksToStart, int downloadDelay, Information informationForUser, PathPack paths)
        { 
            this.informationForUser = informationForUser;

            this.paths = new PathPack(paths.Urlfilepath, paths.Downloadpath + mainFolderName);

            if (!File.ReadAllLines(paths.Urlfilepath).ToList().Equals(null))
            {
                URLs = File.ReadAllLines(paths.Urlfilepath).ToList();
            }
            else
            {
                throw new ArgumentNullException();  
            }

            this.informationForUser.UrlCountReady(URLs.Count.ToString());
            downloadedFiles = new ConcurrentQueue<string>();
            this.tasksToStart = tasksToStart;
            this.downloadDelay = downloadDelay;
            successfullyParsed = 0;
            successfullyDownloaded = 0;

            semaphoreCount = tasksToStart > maxTaskToDownload ? maxTaskToDownload : tasksToStart;
            semaphore = new SemaphoreSlim(semaphoreCount);
            stopwatch = new Stopwatch();

            Directory.CreateDirectory(this.paths.Downloadpath + downloadedFileFolderName);
            Directory.CreateDirectory(this.paths.Downloadpath + folderForJsonFiles);
            Directory.CreateDirectory(this.paths.Downloadpath + folderForLogFiles);
        }

        public void RunSolution()
        {
            Log("");
            Log("**************************************START*********************************************");
            Log("");

            Log("Starting process. . . " + DateTime.Now);
            Log("-Task count for downloading files (2 at maximum): " + semaphoreCount);
            Log("-Task count for extracting data: " + tasksToStart);
            Log("-URL count: " + URLs.Count);
            Log("-Delay between failed downloads: " + downloadDelay);
            Log("");

            stopwatch.Start();
            DownloadPages();
            stopwatch.Stop();

            TimeSpan downloadElapsed = stopwatch.Elapsed;
           
            Log("");

            stopwatch.Restart();
            ParseFiles();

            if (tasksToStart > 1)
            {
                MergeJsonFiles();
            }

            stopwatch.Stop();

            TimeSpan parseElapsed = stopwatch.Elapsed;

            Log("");
            Log("###SUMMARY###");
            Log("");
            Log("Downloading took " + downloadElapsed.Minutes + " minutes, " + downloadElapsed.Seconds + " seconds, " + downloadElapsed.Milliseconds + " milliseconds");
            Log("URL count: " + URLs.Count + ", successfully downloaded: " + successfullyDownloaded);
            Log("");
            Log("Parsing took " + parseElapsed.Minutes + " minutes, " + parseElapsed.Seconds + " seconds, " + parseElapsed.Milliseconds + " milliseconds");
            Log("Downloaded: " + successfullyDownloaded + ", successfully parsed: " + successfullyParsed);
            Log("");
            Log("***************************************END**********************************************");
            informationForUser.ProcessChanged("Finished");
        }

        static bool MoveOn(ref int tryCount)
        {
            tryCount += 1;
            return tryCount < maxTry;
        }

        void DownloadPages()
        {
            informationForUser.ProcessChanged("Downloading pages");

            Log("-------- Current process: " + informationForUser.Processname + " --------");

            Directory.CreateDirectory(paths.Downloadpath);

            Parallel.ForEach(URLs, new ParallelOptions { MaxDegreeOfParallelism = tasksToStart }, (current) =>
            {
                semaphore.Wait();
                int tryCount = 0;
                string html = string.Empty;
                string exceptionMessage = string.Empty;

                do
                {
                    try
                    {
                        using (WebClient client = new WebClient())
                        {
                            html = client.DownloadString(current); //.DownloadString(current);
                            client.Dispose();
                            break;
                        }
                    }
                    catch (WebException e)
                    {                      
                        exceptionMessage = e.Message;
                        BackOff.Backoff(downloadDelay);                      
                    }
                } while (!MoveOn(ref tryCount));

                if (!string.IsNullOrEmpty(html))
                {
                    using (StreamWriter sw = new StreamWriter(paths.Downloadpath + downloadedFileFolderName + current.Split('/').Last() + ".html", false, Encoding.UTF8))
                    {
                        sw.Write(html);
                    }

                    Interlocked.Increment(ref successfullyDownloaded);
                }
                else
                {
                    lock (lockobj)
                    {
                        Log("-Failed to download " + current + " - " + exceptionMessage);
                    }
                }

                lock (lockobj)
                {
                    informationForUser.IncrementProgress();
                }

                semaphore.Release();
            });
        }

        void ParseFiles()
        {
            informationForUser.NullifyProgress();
            informationForUser.ProcessChanged("Extracting data from files");

            Log("-------- Current process: " + informationForUser.Processname + " --------");

            //Removing existing JSON files before starting 
            foreach (string item in Directory.GetFiles(paths.Downloadpath + folderForJsonFiles))
            {
                File.Delete(item);
            }
            Log("");
            Log("*Previous JSON files are deleted*");
            Log("");

            //Gets done fast, no need of parallelism
            foreach (string file in Directory.GetFiles(paths.Downloadpath + downloadedFileFolderName, "*.html"))
            {
                downloadedFiles.Enqueue(file);
            }

            List<Task> parsingTasks = new List<Task>();

            for (int i = 0; i < tasksToStart; i++)
            {              
                parsingTasks.Add(new Task(() =>
                {
                    int index = (int)Task.CurrentId;
                    string actualFile = string.Empty;
                    HtmlDocument actualDocument = new HtmlDocument();
                    List<HtmlNode> helperList = new List<HtmlNode>();
                    HtmlNode helperNode = null;
                    JsonSerializer serializer = new JsonSerializer();

                    Band actualBand = new Band();
                    StreamWriter sw = new StreamWriter(paths.Downloadpath + folderForJsonFiles + separatedJsonFileName + index + ".json");

                    while (!downloadedFiles.IsEmpty)
                    {
                        downloadedFiles.TryDequeue(out actualFile);

                        if (!string.IsNullOrEmpty(actualFile))
                        {
                            actualDocument.Load(actualFile);

                            actualBand = new Band();

                            lock (lockobj)
                            {
                                informationForUser.IncrementProgress();
                            }

                            //If any data is missing then the actual band is invalid => go to next file
                            try
                            {
                                //Band name:
                                actualBand.BandName = actualDocument.DocumentNode.Descendants("span").Where(node => node.Attributes["class"].Value == "fn org").First().InnerText;

                                //Genres:
                                helperNode = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Genres").First();
                                int asd = helperNode.ParentNode.Descendants("a").Count();
                                foreach (HtmlNode node in helperNode.ParentNode.Descendants("a"))
                                {
                                    actualBand.Genres.Add(node.Attributes["title"].Value);
                                }
                            }
                            catch (Exception)
                            {
                                lock (lockobj)
                                {
                                    Log("-Failed to parse " + actualFile + " - " + "(No band name or genres found)");
                                }

                                continue;
                            }

                            //Origin:
                            try
                            {                         
                                helperNode = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Origin").First();
                                foreach (HtmlNode node in helperNode.ParentNode.Descendants("a"))
                                {
                                    actualBand.Origin = node.GetAttributeValue("title", string.Empty) + (node.Equals(helperNode.ParentNode.Descendants("a").Last()) ? string.Empty : ", ");
                                }
                            }
                            catch (Exception)
                            {
                                actualBand.Origin = string.Empty;
                            }

                            //Members:
                            try
                            {
                                helperNode = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Members").First();
                                foreach (HtmlNode node in helperNode.ParentNode.Descendants("a"))
                                {
                                    actualBand.Members.Add(node.Attributes["title"].Value.Replace(" (page does not exist)",""));
                                }
                            }
                            catch (Exception)
                            {
                                //Nothing to do
                            }

                            //Past members:
                            try
                            {
                                helperNode = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Past members").First();
                                foreach (HtmlNode node in helperNode.ParentNode.Descendants("a"))
                                {
                                    actualBand.PastMembers.Add(node.Attributes["title"].Value.Replace(" (page does not exist)", ""));
                                }
                            }
                            catch (Exception)
                            {
                                //Nothing to do
                            }

                            //Website:
                            try
                            {
                                actualBand.Website = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Website").First().ParentNode.Descendants("a").First().Attributes["href"].Value;
                            }
                            catch (Exception)
                            {
                                actualBand.Website = string.Empty;
                            }

                            //Labels:
                            try
                            {
                                helperNode = actualDocument.DocumentNode.Descendants("th").Where(node => node.InnerText == "Labels").First();
                                foreach (HtmlNode node in helperNode.ParentNode.Descendants("a"))
                                {
                                    actualBand.Labels.Add(node.Attributes["title"].Value);
                                }
                            }
                            catch (Exception)
                            {
                                //Nothing to do
                            }

                            Interlocked.Increment(ref successfullyParsed);
                            sw.Write(JsonConvert.SerializeObject(actualBand, Formatting.Indented));
                            sw.WriteLine();
                        }
                    }

                    sw.Close();
                }, TaskCreationOptions.LongRunning));

                parsingTasks[i].Start();
            }

            Task.WaitAll(parsingTasks.ToArray());
        }

        private void MergeJsonFiles()
        {          
            List<string> jsonFiles = Directory.GetFiles(paths.Downloadpath + folderForJsonFiles).ToList();
            StreamWriter sw = new StreamWriter(paths.Downloadpath + folderForJsonFiles + finalJsonFileName);

            StreamReader sr;

            foreach (string file in jsonFiles)
            {
                sr = new StreamReader(file);

                while (!sr.EndOfStream)
                {
                    sw.WriteLine(sr.ReadLine());
                }

                sr.Close();
            }

            sw.Close();
        }

        private void Log(string message)
        {
            using (StreamWriter sw = new StreamWriter(paths.Downloadpath + folderForLogFiles +  logfileName, true))
            {
                sw.WriteLine(message);
            }
        }      
    }
}
