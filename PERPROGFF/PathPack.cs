﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PERPROGFF
{
    /// <summary>
    /// Class for containing the paths
    /// </summary>
    public class PathPack
    {
        string urlfilepath;
        string downloadpath;

        public string Urlfilepath
        {
            get
            {
                return urlfilepath;
            }

            set
            {
                urlfilepath = value;
            }
        }

        public string Downloadpath
        {
            get
            {
                return downloadpath;
            }

            set
            {
                downloadpath = value;
            }
        }

        public PathPack()
        {
            
        }

        public PathPack(string urlfilepath, string downloadpath)
        {
            this.urlfilepath = urlfilepath;
            this.downloadpath = downloadpath;
        }

        public bool IsPackReady()
        {
            return !(string.IsNullOrEmpty(downloadpath) || string.IsNullOrEmpty(urlfilepath));
        }
    }
}
